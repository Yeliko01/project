package Assignment.domain; 
 
public class PoemWriter expand User { 
 private User name; 
 private User surname; 
 private User username; 
 private Password password; 
 private int word; 
 private int synonym; 
 
 public PoemWriter(User name,User surname,User username, Password password){ 
 super(name, "Aruna", "Adilova", surname); 
 } 
 public PoemWriter(User name,User surname,User username, Password password, int word, int synonym){ 
 this(name, surname, username, password); 
 setWord(word); 
 setSynonym(synonym); 
 } 

 @Override 
 public User getname() { 
 return name; 
 } 
 
 @Override
 public User getsurname() { 
 return surname; 
 } 
 
 @Override
 public User getusurname() { 
 return username; 
 } 
 
 @Override
 public Password getpassword() { 
 return password; 
 } 
 
 @Override 
 public void setpassword(Password password) { 
 this.password = password; 
 } 
 
 public void setname(User name) { 
 this.name = name; 
 } 
 
 public void setsurname(User surname) { 
 this.surname = surname; 
 } 
 
 public void setusername(User username) { 
 this.username = username; 
 } 
 
 public int getWord() { 
 return word; 
 } 
 
 public void setWord(int word) { 
 this.word = word; 
 } 
 
 public int getSynonym() { 
 return synonym; 
 } 
 
 public void setSynonym(int synonym) { 
 this.synonym = synonym; 
 } 

 @Override
 public String toString() { 
 return super.toString() + "About Poem Writers." + " her words value are " + Word + ". And synonyms value are " 
 + synonym; 
 } 
}