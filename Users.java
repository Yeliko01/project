package Assignment.domain; 
 
public class User { 
 private int id; 
 private int id_gen=0; 
 private String name; 
 private String surname; 
 private String username; 
 private Password password; 
 
 public User(int id, String name, String surname, String username) { 
 setId(id); 
 setname(name); 
 setsurname(surname); 
 setusername(username); 
 } 
 public User(int id, String name, String surname, String username, Password password){ 
 this(id, name, surname , username); 
 setpassword(password); 
 } 
 
 private void generateId(){ 
 id=id_gen++; 
 } 
 public int getId() { 
 return id; 
 } 
 
 public String getname() { 
 return name; 
 } 
 
 public String getsurname() { 
 return surname; 
 } 
 
 public String getusername() { 
 return username; 
 } 
 
 public Password getpassword() { 
 return password; 
 } 
 
 public void setId(int id) { 
 this.id = id; 
 } 
 
 public void setname(String name) { 
 this.name = name; 
 } 
 
 public void setsurname(String surname) { 
 this.surname = surname; 
 } 
 
 public void setpassword(Password password) { 
 this.password = password; 
 } 
 
 public void setusername(String username) { 
 this.username = username; 
 } 
 
 @Override
 public String toString() { 
 return id + " " + name + " " + surname + " " + username + " " + password; 
 } 
}